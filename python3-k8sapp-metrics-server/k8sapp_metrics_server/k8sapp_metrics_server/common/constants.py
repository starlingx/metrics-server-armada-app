#
# Copyright (c) 2023 Wind River Systems, Inc.
#
# SPDX-License-Identifier: Apache-2.0
#

HELM_NS_METRICS_SERVER = "metrics-server"
HELM_APP_METRICS_SERVER = "metrics-server"
HELM_CHART_METRICS_SERVER = "metrics-server"
FLUXCD_HELMRELEASE_METRICS_SERVER = "metrics-server"
